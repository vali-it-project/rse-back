package com.Recipes.Recipes.Search.Engine;

public class Recipe {
    private int id;
    private String name;
    private String recipeUrl;
    private String recipeImage;

    private int ingredientMatchCount = 0;


    public Recipe(String name, String recipeUrl, int id, String recipeImage) {
        this.name = name;
        this.recipeUrl = recipeUrl;
        this.id = id;
        this.recipeImage = recipeImage;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setRecipeUrl(String recipeUrl) {
        this.recipeUrl = recipeUrl;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public String getRecipeUrl() {
        return recipeUrl;
    }

    public int getId() {
        return id;
    }

    public int getIngredientMatchCount() {
        return ingredientMatchCount;
    }

    public void setIngredientMatchCount(int ingredientMatchCount) {
        this.ingredientMatchCount = ingredientMatchCount;
    }

    public String getRecipeImage() {
        return recipeImage;
    }

    public void setRecipeImage(String recipeImage) {
        this.recipeImage = recipeImage;
    }
}
