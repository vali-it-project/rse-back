-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.12-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for recipes
DROP DATABASE IF EXISTS `recipes`;
CREATE DATABASE IF NOT EXISTS `recipes` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `recipes`;

-- Dumping structure for table recipes.ingredient
DROP TABLE IF EXISTS `ingredient`;
CREATE TABLE IF NOT EXISTS `ingredient` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IngredientName` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

-- Dumping data for table recipes.ingredient: ~59 rows (approximately)
/*!40000 ALTER TABLE `ingredient` DISABLE KEYS */;
INSERT INTO `ingredient` (`ID`, `IngredientName`) VALUES
	(1, 'kartul'),
	(2, 'porgand'),
	(3, 'seesamiseemned'),
	(4, 'küüslauk'),
	(5, 'sibul'),
	(6, 'suvikõrvits / tsukiini'),
	(7, 'baklažaan'),
	(8, 'tomat'),
	(9, 'kurk'),
	(10, 'kaalikas'),
	(11, 'peet'),
	(12, 'kikerherned'),
	(13, 'punased oad'),
	(14, 'mais'),
	(15, 'herned'),
	(16, 'valged oad'),
	(17, 'bataat'),
	(18, 'valge riis'),
	(19, 'pruun riis'),
	(20, 'punane riis'),
	(21, 'pasta (erinevad sordid)'),
	(22, 'sidrun'),
	(23, 'punased läätsed'),
	(24, 'rohelised läätsed'),
	(25, 'mustad läätsed'),
	(26, 'kinoa'),
	(27, 'kuskuss'),
	(28, 'taimne koor'),
	(29, 'taimne piim'),
	(30, 'tofu'),
	(31, 'päikesekuivatatud tomatid'),
	(32, 'oliivid'),
	(33, 'kookospiim'),
	(34, 'paprika'),
	(35, 'porru'),
	(36, 'avokaado'),
	(37, 'granaatõun'),
	(38, 'taimne salatikaste'),
	(39, 'marineeritud kurk'),
	(40, 'kapsas'),
	(41, 'jääsalat / rooma salat'),
	(42, 'salatilehed/salatisegu'),
	(43, 'seened'),
	(44, 'maapähklivõi'),
	(45, 'odrakruubid'),
	(46, 'tatar'),
	(47, 'nuudlid'),
	(48, 'tomatipasta'),
	(49, 'jahu'),
	(50, 'lillkapsas'),
	(51, 'brokkoli'),
	(52, 'tortilla'),
	(53, 'linaseemned'),
	(54, 'päevalilleseemned'),
	(55, 'india pähklid'),
	(56, 'sinep'),
	(57, 'sojakaste / tamari'),
	(58, 'riivsai'),
	(59, 'juurseller');
/*!40000 ALTER TABLE `ingredient` ENABLE KEYS */;

-- Dumping structure for table recipes.recipe
DROP TABLE IF EXISTS `recipe`;
CREATE TABLE IF NOT EXISTS `recipe` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecipeName` varchar(200) NOT NULL,
  `RecipeURL` varchar(500) NOT NULL,
  `RecipeImage` varchar(500) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table recipes.recipe: ~7 rows (approximately)
/*!40000 ALTER TABLE `recipe` DISABLE KEYS */;
INSERT INTO `recipe` (`ID`, `RecipeName`, `RecipeURL`, `RecipeImage`) VALUES
	(1, 'Krõbedad seesami-küüslaugukartulid', 'http://www.taimetoit.ee/2017/03/seesami-kuuslaugukartulid.html', 'https://2.bp.blogspot.com/-Jx7StcF3oy0/WLb-GPt98fI/AAAAAAAAThg/tkLKjB3_fbgI88zmswm2H4slG6geYNf9gCLcB/s640/Seesamikartul.jpg'),
	(2, 'Porgandivrapid pipratofu ja guacamolega', 'http://www.taimetoit.ee/2017/01/porgandivrapid-pipratofu-ja-guacamolega.html', 'https://2.bp.blogspot.com/-xSvaOC6rs1o/WG--NqB4-yI/AAAAAAAATUM/-HVHGbPH4Swczv0aTyxB_JChJbNonODEgCLcB/s640/Vegan%2Bwraps%2Bwith%2Bcarrot%2Bnoodles%252C%2Bpepper%2Btofu%2Band%2Bguacamole%2B3.jpg'),
	(3, 'Taimne "lõhe"supp', 'http://www.taimetoit.ee/2018/01/taimne-lohesupp.html', 'https://2.bp.blogspot.com/-BR_aMCVvBD8/WlOela93gLI/AAAAAAAAUhA/cYYEToFIUuk3jNeAdTZCJS9p5jCt3R6pgCLcBGAs/s1600/Vegan%2Bsalmon%2Bsoup_.jpg'),
	(4, 'Läätse pikkpoiss kapsaga ehk suur kapsarull', 'http://www.taimetoit.ee/2017/03/laatse-pikkpoiss-kapsaga-ehk-suur.html', 'https://4.bp.blogspot.com/-pzUQNFtmjJU/WMqf0qIyTQI/AAAAAAAATkw/GwOMn_7aqN4Ksw0BFzX9UenQQGnS6NAuwCLcB/s640/Kapsa%2Bpikkpoiss.jpg'),
	(5, 'Kreemjas seene püreesupp', 'http://www.taimetoit.ee/2018/03/kreemjas-seene-pureesupp.html', 'https://4.bp.blogspot.com/-sok4rj9WHOE/WrDGe5n1S_I/AAAAAAAAUqY/GceKEiVa07cFG-UbdTPQcRgPUKhhM4gxwCEwYBhgL/s640/Vegan%2Bcream%2Bof%2Bmushroom%2Bsoup%2B2.jpg'),
	(6, 'Kreemjas ja krehvtine vegan Caesari salat', 'http://www.taimetoit.ee/2017/03/kreemjas-ja-krehvtine-vegan-caesari.html', 'https://1.bp.blogspot.com/-d2h7z-o4VSA/WLhE83YaKWI/AAAAAAAATik/AESCA7BaG8AgNIXw99qX9fOY9py-xYw8gCLcB/s640/Vegan%2BCaesar%2Bsalad.jpg'),
	(7, 'Luksuslik tofuburger friikartulitega', 'http://www.taimetoit.ee/2012/12/luksuslik-tofuburger.html', 'http://1.bp.blogspot.com/-Wg1tt6l3r0k/VCvvhVHGd1I/AAAAAAAALBo/CIh28x1Mbtg/s1600/Tofuburger.jpg');
/*!40000 ALTER TABLE `recipe` ENABLE KEYS */;

-- Dumping structure for table recipes.recipeingredient
DROP TABLE IF EXISTS `recipeingredient`;
CREATE TABLE IF NOT EXISTS `recipeingredient` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecipeID` varchar(45) NOT NULL,
  `IngredientID` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- Dumping data for table recipes.recipeingredient: ~38 rows (approximately)
/*!40000 ALTER TABLE `recipeingredient` DISABLE KEYS */;
INSERT INTO `recipeingredient` (`ID`, `RecipeID`, `IngredientID`) VALUES
	(1, '1', '1'),
	(2, '1', '3'),
	(3, '1', '4'),
	(4, '2', '30'),
	(5, '2', '2'),
	(6, '2', '36'),
	(7, '2', '5'),
	(8, '2', '8'),
	(9, '2', '52'),
	(10, '3', '5'),
	(11, '3', '17'),
	(12, '3', '1'),
	(13, '3', '28'),
	(14, '4', '40'),
	(15, '4', '24'),
	(16, '4', '18'),
	(17, '4', '5'),
	(18, '4', '53'),
	(19, '5', '5'),
	(20, '5', '4'),
	(21, '5', '43'),
	(22, '5', '1'),
	(23, '5', '54'),
	(24, '6', '55'),
	(25, '6', '4'),
	(26, '6', '56'),
	(27, '6', '57'),
	(28, '6', '41'),
	(29, '6', '12'),
	(30, '2', '57'),
	(31, '7', '30'),
	(32, '7', '58'),
	(33, '7', '41'),
	(34, '7', '2'),
	(35, '7', '59'),
	(36, '7', '57'),
	(37, '7', '43'),
	(38, '7', '1');
/*!40000 ALTER TABLE `recipeingredient` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
